#!/usr/bin/env bash
echo '------------------------------------------'
echo 'Hello, Welcome to Harpers Git Source Build'
echo 'https://gitlab.com/m0ze1r/gitbuild'
echo 'This is for debian/ubuntu based systems'
echo '------------------------------------------'
sleep 2
echo '------------------------------------------'
echo 'Purging git via apt from system'
echo '------------------------------------------'
##This is to purge any remnants of git from the system
sudo apt-get --purge remove git
#git has been purged!
echo '------------------------------------------'
echo 'git has been purged!'
echo '------------------------------------------'
#making sure we update all the things
echo '------------------------------------------'
echo 'apt updating'
echo '------------------------------------------'
sleep 1
sudo apt-get update
#update time!
echo '------------------------------------------'
echo 'installing wget make libssl libghc libcur14 libexpat gettext and unzip'
echo '------------------------------------------'
#
sleep 1
#getting all the packages we need
sudo apt-get install wget make libssl-dev libghc-zlib-dev libcurl4-gnutls-dev libexpat1-dev gettext unzip
#
echo '------------------------------------------'
echo 'making dir to put the latest git into and giving it permissions'
echo '------------------------------------------'
# cd'ing to tmp so we are safe about how we install things
sleep 1
cd /tmp
sleep 1
#making dir for git to wget to
sudo mkdir latest-git
sleep 1
#giving permissions for the $USER to execute into
sudo chown $USER:$USER /tmp/latest-git
sleep 1
cd /tmp/latest-git
#
echo '------------------------------------------'
echo 'wget the latest git'
echo '------------------------------------------'
sudo wget https://github.com/git/git/archive/refs/heads/master.zip
#
echo '------------------------------------------'
echo 'unzipping master git'
echo '------------------------------------------'
unzip master.zip
#here comes the fun part
cd /tmp/latest-git/git-master/
#
echo '------------------------------------------'
echo 'making the latest git'
echo '------------------------------------------'
#MAKE time!
sudo make prefix=/usr/local all
#
sudo make prefix=/usr/local install
#
echo '------------------------------------------'
git --version
echo '------------------------------------------'
#hopefully this worked?
echo '------------------------------------------'
echo 'git should be the latest version now'
echo 'doing some cleaning up'
echo '------------------------------------------'
cd /tmp
sudo rm -rf /tmp/latest-git
#
echo '------------------------------------------'
echo 'system clean, enjoy your day'
echo '------------------------------------------'
# gitlab.com/m0ze1r/gitbuild.git
# gitea.whisper.cat/harper/gitbuild.git

